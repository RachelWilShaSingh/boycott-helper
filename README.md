# About

I'm compiling data about brands and companies that own them,
as well as doing research into the ethics of those companies.
I try to avoid stuff like Nestle as much as possible,
but I also didn't realize how many brands they own.

I'm storing the data in Comma Separated Value (CSV) files
so the data can be used for any kind of application.

Feel free to make updates and create a Pull Request
and I can merge things in.
